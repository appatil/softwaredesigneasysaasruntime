package esu.asu.easysaasruntime.model;

import java.util.LinkedHashMap;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tenantInformation")
public class TenantInformation {

	private String username;

	private String tenantName;

	private String tenantId;

	private LinkedHashMap<String, String> tenantFields;

	public TenantInformation() {
		tenantFields = new LinkedHashMap();
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public LinkedHashMap<String, String> getTenantFields() {
		return tenantFields;
	}

	public void setTenantFields(LinkedHashMap<String, String> tenantFields) {
		this.tenantFields = tenantFields;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "TenantInformation [username=" + username + ", tenantName="
				+ tenantName + ", tenantId=" + tenantId + ", tenantFields="
				+ tenantFields + "]";
	}
}
