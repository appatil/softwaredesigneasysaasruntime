package esu.asu.easysaasruntime.model;

import java.util.ArrayList;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tenantData")
public class TenantData {

	private String tenantName;

	private String tenantId;

	private ArrayList<ArrayList<String>> tenantEntries;

	public TenantData() {
		tenantEntries = new ArrayList<>();
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public ArrayList<ArrayList<String>> getTenantEntries() {
		return tenantEntries;
	}

	public void setTenantEntries(ArrayList<ArrayList<String>> tenantFields) {
		this.tenantEntries = tenantEntries;
	}

	@Override
	public String toString() {
		return "TenantData [tenantName=" + tenantName + ", tenantId="
				+ tenantId + ", tenantEntries=" + tenantEntries + "]";
	}

}
