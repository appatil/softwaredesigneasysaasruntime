package esu.asu.easysaasruntime.driver;

import java.util.ArrayList;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import esu.asu.easysaasruntime.model.TenantData;
import esu.asu.easysaasruntime.mongoconfig.SpringMongoConfig;

public class TenantCrud {

	private MongoOperations mongoOperation;

	private ApplicationContext ctx;

	public TenantCrud() {
		ctx = new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
	}

	public static void main(String args[]) {

		TenantCrud tenantCrud = new TenantCrud();
		tenantCrud.getAllTenantRecords("124");
		ArrayList list = new ArrayList();
		list.add("shreyas");
		list.add("5557778889");
		list.add("shreyas@asu.com");

		// adding new record
		tenantCrud.addTenantRecord("124", list);

		// deleting record
		tenantCrud.deleteTenantRecord("124", list);

		ArrayList<ArrayList<String>> allRecords = new ArrayList();

		ArrayList updated = new ArrayList();
		updated.add("anne");
		updated.add("7777711111");
		updated.add("anne@asu.com");

		allRecords.add(updated);

		tenantCrud.updateTenantRecord("124", allRecords);

	}

	public TenantData getAllTenantRecords(String tenantId) {
		// query to search user
		Query searchUserQuery = new Query(Criteria.where("tenantId").is(
				tenantId));

		System.out.println(" Tenant Data for Tenant Grocery List  is ");
		TenantData tenantData = mongoOperation.findOne(searchUserQuery,
				TenantData.class);

		System.out.println(tenantData);
		System.out.println(" Data Entries ");
		ArrayList<ArrayList<String>> tenantEntries = tenantData
				.getTenantEntries();

		for (ArrayList<String> eachEntry : tenantEntries) {
			for (String eachValue : eachEntry) {
				System.out.println(eachValue);
			}

			System.out.println();
		}
		return tenantData;
	}

	public void addTenantRecord(String tenantId, ArrayList<String> newEntry) {

		// query to search user
		Query searchUserQuery = new Query(Criteria.where("tenantId").is(
				tenantId));

		System.out.println(" Before Adding  New  Entry");

		TenantData tenantData = getAllTenantRecords(tenantId);

		ArrayList<ArrayList<String>> newRecord = tenantData.getTenantEntries();

		newRecord.add(newEntry);

		System.out.println(" After Adding  New  Entry");

		// update password
		mongoOperation.updateFirst(searchUserQuery,
				Update.update("tenantEntries", newRecord), TenantData.class);

		getAllTenantRecords(tenantId);

	}

	public void updateTenantRecord(String tenantId,
			ArrayList<ArrayList<String>> updatedRecords) {

		// query to search user
		Query searchUserQuery = new Query(Criteria.where("tenantId").is(
				tenantId));

		System.out.println(" Before Updating  Entries");

		TenantData tenantData = getAllTenantRecords(tenantId);

		System.out.println(" After Updating Entries ");

		// update password
		mongoOperation.updateFirst(searchUserQuery,
				Update.update("tenantEntries", updatedRecords),
				TenantData.class);

		getAllTenantRecords(tenantId);

	}

	public void deleteTenantRecord(String tenantId, ArrayList<String> newEntry) {

		// query to search user
		Query searchUserQuery = new Query(Criteria.where("tenantId").is(
				tenantId));

		System.out.println(" Before Deleting Entry");

		TenantData tenantData = getAllTenantRecords(tenantId);

		ArrayList<ArrayList<String>> allRecords = tenantData.getTenantEntries();

		ArrayList<String> toRemove = null;

		for (ArrayList<String> eachEntry : allRecords) {

			if (eachEntry.equals(newEntry)) {
				toRemove = eachEntry;
				break;
			}
		}

		if (toRemove != null) {
			allRecords.remove(toRemove);
		}
		System.out.println(" After Deleting  Entry");

		// update password
		mongoOperation.updateFirst(searchUserQuery,
				Update.update("tenantEntries", allRecords), TenantData.class);

		getAllTenantRecords(tenantId);

	}
}
