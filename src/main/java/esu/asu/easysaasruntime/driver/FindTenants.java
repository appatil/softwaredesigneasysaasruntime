package esu.asu.easysaasruntime.driver;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import esu.asu.easysaasruntime.model.TenantInformation;
import esu.asu.easysaasruntime.mongoconfig.SpringMongoConfig;

public class FindTenants {

	public static void main(String args[]) {

		new FindTenants().findTenants("aniket");
	}

	public List findTenants(String username) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		// query to search user
		Query searchUserQuery = new Query(Criteria.where("username").is(
				username));

		// find the first Tenant.

		System.out.println(" First Tenant for user aniket is ");
		TenantInformation tenantInformation = mongoOperation.findOne(
				searchUserQuery, TenantInformation.class);

		System.out.println(tenantInformation);

		// finds all Tenants.

		System.out.println(" All the tenants for user aniket are  ");
		List<TenantInformation> tenants = mongoOperation.find(searchUserQuery,
				TenantInformation.class);

		for (TenantInformation tenant : tenants) {
			System.out.println(tenant);
		}

		return tenants;
	}
}
