package esu.asu.easysaasruntime.driver;

import java.util.ArrayList;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;

import esu.asu.easysaasruntime.model.TenantData;
import esu.asu.easysaasruntime.mongoconfig.SpringMongoConfig;

public class TenantDataTest {

	public static void main(String args[]) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		TenantData tenantData1 = new TenantData();

		tenantData1.setTenantName("Contacts");

		tenantData1.setTenantId("124");

		ArrayList tenantEntries = tenantData1.getTenantEntries();

		ArrayList<String> item1 = new ArrayList<>();

		item1.add("aniket");

		item1.add("4804064766");

		item1.add("appatil@asu.edu");

		ArrayList<String> item2 = new ArrayList<>();

		item2.add("derek");

		item2.add("5556667778");

		item2.add("derek@asu.edu");

		tenantEntries.add(item1);

		tenantEntries.add(item2);

		tenantData1.setTenantEntries(tenantEntries);

		mongoOperation.save(tenantData1);

	}

}
