package esu.asu.easysaasruntime.driver;

import java.util.LinkedHashMap;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;

import esu.asu.easysaasruntime.model.TenantInformation;
import esu.asu.easysaasruntime.mongoconfig.SpringMongoConfig;

public class TenantTest {

	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(
				SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx
				.getBean("mongoTemplate");

		TenantInformation tenant1 = new TenantInformation();

		tenant1.setUsername("aniket");

		tenant1.setTenantName("GroceryList");

		tenant1.setTenantId("123");

		LinkedHashMap<String, String> tenantFields = tenant1.getTenantFields();

		tenantFields.put("ItemName", "String");

		tenantFields.put("Quantity", "int");

		tenant1.setTenantFields(tenantFields);

		// mongoOperation.save(tenant1);

		TenantInformation tenant2 = new TenantInformation();

		tenant2.setUsername("aniket");

		tenant2.setTenantName("Contacts");

		tenant2.setTenantId("124");

		LinkedHashMap<String, String> tenantFields2 = tenant2.getTenantFields();

		tenantFields2.put("Name", "String");

		tenantFields2.put("Phone", "int");

		tenantFields2.put("Email", "String");

		tenant2.setTenantFields(tenantFields2);

		mongoOperation.save(tenant2);

	}
}
